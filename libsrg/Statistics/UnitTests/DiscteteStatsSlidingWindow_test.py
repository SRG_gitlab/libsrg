# libsrg (Code and Documentation) is published under an MIT License
# Copyright (c) 2023,2024 Steven Goncalo
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
import time
import unittest
from datetime import datetime
from enum import Enum

from libsrg.Statistics.DiscreteStatsSlidingWindow import DiscreteStatsSlidingWindow


class Status(Enum):
    NODATA = (-1, 'grey', 'lightgrey')
    OK = (0, 'lime', 'lightgreen')
    UNKNOWN = (1, 'magenta', '#f1a7fe')
    MAINT = (2, 'cyan', '#e0ffff')
    WARNING = (3, 'yellow', 'lightyellow')
    CRITICAL = (4, 'red', '#ffc0cb')


class MyTestCase(unittest.TestCase):
    def test_something(self):
        ds = DiscreteStatsSlidingWindow("Colors")
        now = time.time()
        ds.sample("red", sample_time=now)
        ds.sample("red", sample_time=now)
        ds.sample("red", sample_time=now)
        ds.sample("blue", sample_time=now)
        ds.sample("blue", sample_time=now)
        ds.sample("green", sample_time=now)



        self.assertEqual(3, ds.count_for("red"))
        self.assertEqual(2, ds.count_for("blue"))
        self.assertEqual(1, ds.count_for("green"))
        self.assertEqual(0, ds.count_for("yellow"))

        print(ds.counts())
        act = ds.most_common(2)
        # print(ds.most_common())
        print(ds.most_common(1))
        print(ds.most_common(2))
        print(ds.most_common(3))
        print(ds.most_common(4))

        # timetags not comparing
        exp = [('red', 3, now), ('blue', 2, now)]
        for act1,ex1 in zip(act, exp):
            self.assertEqual(ex1[0], act1[0])
            self.assertEqual(ex1[1], act1[1])

    def test_status(self):
        ds = DiscreteStatsSlidingWindow("Status")
        ds.sample(Status.OK)
        ds.sample(Status.OK)
        ds.sample(Status.CRITICAL)
        self.assertEqual(2, ds.count_for(Status.OK))
        self.assertEqual(1, ds.count_for(Status.CRITICAL))
        self.assertEqual(0, ds.count_for(Status.WARNING))
        print(ds.counts())
        print(ds.most_common(3))
        print(ds.most_common_as_str(3))

    def test_window(self):
        ds = DiscreteStatsSlidingWindow("Status", window=3)
        ds.sample(Status.OK)
        ds.sample(Status.OK)
        ds.sample(Status.CRITICAL)
        self.assertEqual(2, ds.count_for(Status.OK))
        self.assertEqual(1, ds.count_for(Status.CRITICAL))
        self.assertEqual(0, ds.count_for(Status.WARNING))

        ds.sample(Status.WARNING)
        self.assertEqual(1, ds.count_for(Status.OK))
        self.assertEqual(1, ds.count_for(Status.CRITICAL))
        self.assertEqual(1, ds.count_for(Status.WARNING))

        ds.sample(Status.WARNING)
        self.assertEqual(0, ds.count_for(Status.OK))
        self.assertEqual(1, ds.count_for(Status.CRITICAL))
        self.assertEqual(2, ds.count_for(Status.WARNING))

        ds.sample(Status.WARNING)
        self.assertEqual(0, ds.count_for(Status.OK))
        self.assertEqual(0, ds.count_for(Status.CRITICAL))
        self.assertEqual(3, ds.count_for(Status.WARNING))


if __name__ == '__main__':
    unittest.main()
