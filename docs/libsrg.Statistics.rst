libsrg.Statistics package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   libsrg.Statistics.UnitTests

Submodules
----------

libsrg.Statistics.ADStatsBase module
------------------------------------

.. automodule:: libsrg.Statistics.ADStatsBase
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.AnalogStatsBase module
----------------------------------------

.. automodule:: libsrg.Statistics.AnalogStatsBase
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.AnalogStatsCumulative module
----------------------------------------------

.. automodule:: libsrg.Statistics.AnalogStatsCumulative
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.AnalogStatsFading module
------------------------------------------

.. automodule:: libsrg.Statistics.AnalogStatsFading
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.AnalogStatsSlidingWindow module
-------------------------------------------------

.. automodule:: libsrg.Statistics.AnalogStatsSlidingWindow
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.DiscreteStatsBase module
------------------------------------------

.. automodule:: libsrg.Statistics.DiscreteStatsBase
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.DiscreteStatsCumulative module
------------------------------------------------

.. automodule:: libsrg.Statistics.DiscreteStatsCumulative
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.DiscreteStatsSlidingWindow module
---------------------------------------------------

.. automodule:: libsrg.Statistics.DiscreteStatsSlidingWindow
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: libsrg.Statistics
   :members:
   :undoc-members:
   :show-inheritance:
