libsrg.Statistics.UnitTests package
===================================

Submodules
----------

libsrg.Statistics.UnitTests.ADStatsBase\_test module
----------------------------------------------------

.. automodule:: libsrg.Statistics.UnitTests.ADStatsBase_test
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.UnitTests.AnalogStatsBase\_test module
--------------------------------------------------------

.. automodule:: libsrg.Statistics.UnitTests.AnalogStatsBase_test
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.UnitTests.AnalogStatsCumulative\_test module
--------------------------------------------------------------

.. automodule:: libsrg.Statistics.UnitTests.AnalogStatsCumulative_test
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.UnitTests.AnalogStatsFading\_test module
----------------------------------------------------------

.. automodule:: libsrg.Statistics.UnitTests.AnalogStatsFading_test
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.UnitTests.AnalogStatsSlidingWindow\_test module
-----------------------------------------------------------------

.. automodule:: libsrg.Statistics.UnitTests.AnalogStatsSlidingWindow_test
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.UnitTests.DiscteteStatsBase\_test module
----------------------------------------------------------

.. automodule:: libsrg.Statistics.UnitTests.DiscteteStatsBase_test
   :members:
   :undoc-members:
   :show-inheritance:

libsrg.Statistics.UnitTests.DiscteteStatsSlidingWindow\_test module
-------------------------------------------------------------------

.. automodule:: libsrg.Statistics.UnitTests.DiscteteStatsSlidingWindow_test
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: libsrg.Statistics.UnitTests
   :members:
   :undoc-members:
   :show-inheritance:
