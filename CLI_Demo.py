# libsrg (Code and Documentation) is published under an MIT License
# Copyright (c) 2023,2024 Steven Goncalo
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import logging

from libsrg.Runner import Runner

from libsrg.LoggingAppBase import LoggingAppBase
from libsrg.LoggingCounter import LoggingCounter

"""
This is just a simple demo application to show how the LoggingAppBase class should be extended.

It serves no real purpose other than serving as a regression test.

pytest mucks with logging before running test code
"""


class SampleApp(LoggingAppBase):

    def __init__(self):
        super().__init__()
        self.logger.info("before adding args")
        # setup any program specific command line arguments
        self.parser.add_argument('--zap', help="Zap something", dest='zap', action='store_true', default=False)
        self.parser.add_argument('--zip', help="Zip something", dest='zip', action='store_true', default=False)
        # invoke the parser
        self.perform_parse()
        #
        self.logger.info(f"after parsing {self.args}")

    def demo_levels(self):
        ctr = LoggingCounter.get_instance()
        self.logger.info(f"getEffectiveLevel is {self.logger.getEffectiveLevel()}  logging.INFO={logging.INFO}")
        oldcount = ctr.count_for_level(logging.DEBUG)
        self.logger.info("call to debug below will be suppressed")
        self.logger.debug("debug log wont show or count this line")
        newcount = ctr.count_for_level(logging.DEBUG)
        assert oldcount == newcount
        self.logger.setLevel(logging.DEBUG)
        self.logger.info("Changed level to DEBUG")
        self.logger.info(f"getEffectiveLevel is {self.logger.getEffectiveLevel()} logging.DEBUG={logging.DEBUG}")
        self.logger.debug("This should show and count")
        newcount = ctr.count_for_level(logging.DEBUG)
        assert (oldcount + 1) == newcount

    def demo_runner(self):
        self.logger.warning("A warning")
        try:
            # linux with systemd assumed in self-test
            #   this is just an external command with multiple lines of output
            r = Runner(["hostnamectl"])
            self.logger.info(r)
            r2 = Runner(["missing program trapped exception"])
            self.logger.info(r2)
            r3 = Runner(["missing program rethrow exception"], rethrow=True)
            self.logger.info(r3)
        except Exception as ex:
            self.logger.info("VVVVV Exception optionally propagated to calling program")
            self.logger.critical(ex, exc_info=True)
            self.logger.info("^^^^^ that was supposed to throw an exception")

    def demo_final_checks(self):
        ctr = LoggingCounter.get_instance()
        self.logger.info("Asserts check actual versus expected logging counts as logged at end of run (atexit)")
        self.logger.info("  note that counters are frozen in atexit code, so atexit output does not change counts\n\n")
        assert ctr.count_for_level(logging.CRITICAL) == 1
        assert ctr.count_for_level(logging.ERROR) == 2
        assert ctr.count_for_level(logging.WARNING) == 1
        assert ctr.count_for_level(logging.DEBUG) == 1
        assert ctr.count_for_level(logging.INFO) == 12

    @classmethod
    def demo(cls):
        app = SampleApp()
        app.demo_levels()
        app.demo_runner()
        app.demo_final_checks()


if __name__ == '__main__':
    SampleApp.demo()
