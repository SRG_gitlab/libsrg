# libsrg (Code and Documentation) is published under an MIT License
# Copyright (c) 2023,2024 Steven Goncalo
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.


"""
MarkLicense starts in the directory it is in, and recursively looks for python files there and in subdirectories.

In each file it finds, the header shown below is inserted after an optional #! hashbang.
If the first 10 characters of the existing text match the header in these lines, they are overwritten rather than inserted.
If the header is already in the file, it remains unchanged.
If dates change, the replace mode can update them.

directories starting with ".", "venv" or "VENV" are skipped.

"""

from pathlib import Path

header = [
    "# libsrg (Code and Documentation) is published under an MIT License\n",
    "# Copyright (c) 2023,2024 Steven Goncalo\n",
    "# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
]


def main():
    here = Path(__file__).parent
    process_dir(here)


def process_file(py_file: Path):
    print(py_file)
    with open(py_file, 'r') as f:
        lines = f.readlines()
    offset=0
    if len(lines)>=1 and lines[0].startswith('#!'):
        offset = 1
    for head in header:
        lead= head[:10]
        if len(lines)>=offset+1 and lines[offset].startswith(lead):
            lines[offset] = head
        else:
            lines.insert(offset, head)
        offset += 1
    with open(py_file, 'w') as f:
        f.writelines(lines)


def process_dir(dir_: Path):
    print(dir_)
    for py_file in dir_.glob("*.py"):
        process_file(py_file)
    for sub_dir in dir_.iterdir():
        name=sub_dir.stem
        if name.startswith((".","venv","VENV")):
            continue
        if sub_dir.is_dir():
            process_dir(sub_dir)

if __name__ == '__main__':
    main()