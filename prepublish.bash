#! /bin/bash

cd /GNAS/PROJ/PycharmProjects/libsrg || exit
rm -rf dist
rm -rf build
#pip install -U build
#pip install -U twine
python3 -m build --wheel
python3 -m twine check dist/*
# python3 -m twine upload dist/*